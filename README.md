# README #

## Introduction:  

This is the second summer projects I will be developing using Ruby on Rails. Primary aim of these projects is to focus on learning Ruby on Rails and apply my knowledge to implement complex features in my web application.

Some of the features implemented are:

* Authentication system for signing-up, editing profile and logging out.
* Users can track up-to 10 financial stocks from YAHOO Stocks.
* Add stocks to portfolio and search new stock symbols.
* Users can search up other users on the platform.
* Users can add and remove friends.
* Users can view the stocks being tracked by their friends and other users.

## Technologies: ##

* HTML/ERB Templating
* CSS/Bootstrap
* Ruby on Rails

## Purpose: ##

* Become proficient in Ruby on Rails.
* Be well equipped for my Fall '16 internship with Shopify.

## Procedure: ##

1. Install [Ruby on Rails.](http://rubyonrails.org/)
2. Browse to the root folder and run the following command in the terminal: rails server -p 0.0.0.0 or rails server


## Timestamp: ##

**July, 2016